<?php

/* On inclu la configuration */
require('config/config.php');

/** On inclu les librairies */
require('lib/db.php');


/** Librairie APP */
require('lib/app.php');


/** Inclure le model User */
require('models/commentaire.php');
    
	


$view ="contact";
$pageTitle = 'Contact';





/** Les erreurs eventuelles lors de la récupération du formulaire */
$errors = [];


// On a besoin d'une instance du model COMMENTAIRE
$commentModel = new Comment();





 var_dump($_POST);

// Vérifier si le formulaire est soumis 
if ( isset( $_POST['email'] ) ) {
    /* récupérer les données du formulaire en utilisant 
       la valeur des attributs name comme clé 
    */
    $firstname = $_POST['firstname']; 
    $lastname= $_POST['lastname']; 
    $email = $_POST['email'];
    $comment = $_POST['comment'];
    $valid = (isset($_POST['valid']))?true:false;

 /** On teste s'il y a des erreurs sur les champs du formulaire  */

// Validation du Champ email
    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $errors['email'] = true;
}
// L'email est-il déjà dans la base ??
    // On ne peut pas avoir 2 emails identique !
    if(!empty($commentModel->getByEmail($email))) {
        $errors['email deja utiliser'] = true;
    }
     // validation du champ firstname
     if(empty($firstname) || strlen($firstname) < 3) {
        $errors['firstname'] = true;
    }
    // validation du champ lastname
    if (empty($lastname) || strlen($lastname) < 3) {
        $errors['lastname'] = true;
    }
     // validation du champ commentaire
     if (empty($comment) || strlen($comment) < 3) {
        $errors['comment'] = true;
     }

         //s'il n'y a pas d'erreur 
     if(empty($errors)) {
        connect();
         $commentModel->add($firstname, $lastname, $email, $comment);

         //Ajout du message flash
         addFlashBag('L\'utilisateur a bien été ajoutée !', 'success');
  }
 

}
/** Appel à la vue pour générer l'HTML */

include('../views/layout.phtml');
 