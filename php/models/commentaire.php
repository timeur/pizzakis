<?php



/**
 * Comment
 */
class Comment {
    
    private $dbh;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->dbh = connect();
    }

    public function getAll() {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM commentaire');

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }


    /** Retourne un commentaire correspondant à l'id passé en param
     * @param int $id l'id recherché dans la table commentaire
     */
    public function getById(int $id)
    {
        try {

            $sth = $this->dbh->prepare('SELECT * FROM commentaire WHERE c_id = :id');

            $sth->bindValue('id', $id, PDO::PARAM_INT);

            $sth->execute();

            $values = $sth->fetch();

            return $values;

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /** Retourne un commentaire correspondant à l'email passé en param
     * @param string $email l'email recherché dans la table commentaire
     * @param  int|null $excludeId
     */
    public function getByEmail(string $email, int $excludeId=null) {
        try {

            $sth = $this->dbh->prepare('SELECT * 
            FROM commentaire 
            WHERE c_email = :email AND c_id != :id');

            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('id', $excludeId, PDO::PARAM_INT);

            $sth->execute();

            $values = $sth->fetch();

            return $values;

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }


    /** Ajoute un commentaire dans la base de donnée
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $comment
    *
     * 
     * @return mixed
     */
    public function add(string $firstname, string $lastname, string $email, string $comment) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO commentaire (c_first_name, c_last_name, c_email, c_comment) 
                VALUES (:firstname,:lastname, :email, :comment )');


            //3. Lier les données
            $sth->bindValue('firstname', $firstname, PDO::PARAM_STR);
            $sth->bindValue('lastname', $lastname, PDO::PARAM_STR);
            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('comment', $comment, PDO::PARAM_STR);
            

            //4. Executer ma requête
            $sth->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    public function update(int $id, string $firstname, string $lastname, string $email, string $comment)
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('UPDATE comment 
            SET c_first_name=:firstname, c_last_name=:lastname, c_email=:email, c_comment=:comment
            WHERE c_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);
            $sth->bindValue('firstname', $firstname, PDO::PARAM_STR);
            $sth->bindValue('lastname', $lastname, PDO::PARAM_STR);
            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('comment', $comment, PDO::PARAM_STR);
        

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }
}
    